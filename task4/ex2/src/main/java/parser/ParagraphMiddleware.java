package parser;

import dataModal.CompoundTextElement;
import dataModal.Paragraph;
import dataModal.Sentence;
import dataModal.TextElement;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ParagraphMiddleware extends Middleware {
    File file;
    private static final String PARAGRAPH_SPLIT_REGEX = "\\s{2}";

    public ParagraphMiddleware(File file) {
        this.file = file;
    }

    @Override
    public List<CompoundTextElement> getData(List<CompoundTextElement> textElement) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        String text = "";
        while ((st = br.readLine()) != null) {
            text += st;
        }
        List<CompoundTextElement> compoundTextElements = new ArrayList<CompoundTextElement>();

        String[] paragraphs = text.trim().split(PARAGRAPH_SPLIT_REGEX);

        for (String paragraph : paragraphs) {
            compoundTextElements.add(new Paragraph(paragraph));
        }
        textElement.addAll(compoundTextElements);


        return checkNext(textElement);
    }
}
