package parser;

import dataModal.*;

import javax.xml.soap.Text;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SentenceMiddleware extends Middleware {
    File file;
    private static final String SENTENCE_SPLIT_REGEX = "\\.";

    public SentenceMiddleware(File file) {
        this.file = file;
    }

    @Override
    public List<CompoundTextElement> getData(List<CompoundTextElement> textElement) throws IOException {
        List<CompoundTextElement> allSentences = new ArrayList<CompoundTextElement>();
        for (int i = 0; i < textElement.size(); i++) {
            CompoundTextElement x = textElement.get(i);
            String paragraph = ((Paragraph) x).getParagraph();
            String sentences[] = paragraph.trim().split(SENTENCE_SPLIT_REGEX);
            List<CompoundTextElement> compoundTextElement = new ArrayList<CompoundTextElement>();
            for (String sentence : sentences) {
                compoundTextElement.add(new Sentence(sentence.trim()));
                textElement.get(i).add(new Sentence(sentence.trim()));
            }

        }
        return checkNext(textElement);
    }
}
