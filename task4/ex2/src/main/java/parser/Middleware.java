package parser;

import dataModal.CompoundTextElement;
import dataModal.TextElement;

import javax.xml.soap.Text;
import java.io.IOException;
import java.util.List;

public abstract class Middleware {
    private Middleware next;

    public Middleware linkWith(Middleware next) {
        this.next = next;
        return next;
    }

    public abstract List<CompoundTextElement> getData(List<CompoundTextElement> textElement) throws IOException;

    protected List<CompoundTextElement> checkNext(List<CompoundTextElement> textElement) throws IOException {
        if (next == null) {
            return null;
        }
        return next.getData(textElement);
    }
}
