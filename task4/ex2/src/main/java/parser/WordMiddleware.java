package parser;

import dataModal.*;

import javax.xml.soap.Text;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordMiddleware extends Middleware {
    File file;
    private static final String WORD_SPLIT_REGEX = "\\s";

    public WordMiddleware(File file) {
        this.file = file;
    }

    @Override
    public List<CompoundTextElement> getData(List<CompoundTextElement> textElement) throws IOException {
        for (CompoundTextElement paragraph : textElement) {
            for (TextElement sentenceElement : paragraph.getChildren()) {
                Sentence sentence = (Sentence) sentenceElement;
//                System.out.println(sentence.getSentence());
                // append compound to sentence
                String words[] = sentence.getSentence().split(WORD_SPLIT_REGEX);
                for (String word : words) {
                    sentence.add(new Word(word));
                }
            }
        }

        return null;
    }
}
