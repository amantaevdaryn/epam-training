package dataModal;

public class Word implements TextElement {
    String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public String toString() {
        return word + " ";
    }

    public int getNumberOfSymbols() {
        return word.length();
    }
}
