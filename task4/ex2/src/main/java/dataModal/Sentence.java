package dataModal;

public class Sentence extends CompoundTextElement {
    private String sentence;

    public Sentence(String sentence) {
        super();
        this.sentence = sentence;
    }

    public String getSentence() {
        return sentence;
    }

    @Override
    public String toString() {
        StringBuffer res = new StringBuffer("");
        for (TextElement compoundTextElement : children) {
            res.append(compoundTextElement);
        }
        res.append('\n');
        return String.valueOf(res);
    }
}
