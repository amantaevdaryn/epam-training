package dataModal;

import java.util.ArrayList;
import java.util.List;

public class CompoundTextElement implements TextElement {
    protected List<TextElement> children = new ArrayList<TextElement>();

    public CompoundTextElement() {
    }

    public CompoundTextElement(List<TextElement> children) {
        add(children);
    }

    public void add(TextElement component) {
        children.add(component);
    }

    public void add(List<TextElement> components) {
        children.addAll(components);
    }

    public void getAll() {
        for (TextElement child : children) {
            System.out.println(child);
        }
        children.add(new Paragraph());
    }

    public List<TextElement> getChildren() {
        return children;
    }

    public int getNumberOfSymbols() {
        int numberOfSymbols = 0;
        for (TextElement textElement : children) {
            numberOfSymbols += textElement.getNumberOfSymbols();
        }
        return numberOfSymbols;
    }

}
