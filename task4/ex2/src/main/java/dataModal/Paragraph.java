package dataModal;

public class Paragraph extends CompoundTextElement {
    private String paragraph;

    public Paragraph() {

    }

    public Paragraph(String paragraph) {
        super();
        this.paragraph = paragraph;
    }

    @Override
    public void getAll() {
        for (TextElement p : children) {
            System.out.println(p);
        }
    }

    public String getParagraph() {
        return paragraph;
    }

    @Override
    public String toString() {
        StringBuffer res = new StringBuffer();
        for (TextElement compoundTextElement : children) {
            res.append(compoundTextElement);
        }
        return String.valueOf(res);
    }
}
