import dataModal.*;
import parser.Middleware;
import parser.ParagraphMiddleware;
import parser.SentenceMiddleware;
import parser.WordMiddleware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final Logger logger = LogManager.getLogger();

    public static void printData(List<CompoundTextElement> data) {
        String res = "";
        for (CompoundTextElement value : data) {
            res += value.toString();
        }
        logger.info(res);
    }

    public static List<CompoundTextElement> copyData(List<CompoundTextElement> values) {
        List<CompoundTextElement> values2 = new ArrayList<CompoundTextElement>();
        for (int i = 0; i < values.size(); i++) {
            Paragraph paragraph = (Paragraph) values.get(i);
            values2.add(new Paragraph(paragraph.getParagraph()));
            for (int j = 0; j < paragraph.getChildren().size(); j++) {
                Sentence sentence = (Sentence) paragraph.getChildren().get(j);
                Sentence newSentence = new Sentence(sentence.getSentence());
                for (int m = 0; m < sentence.getChildren().size(); m++) {
                    Word word = (Word) sentence.getChildren().get(m);
                    Word newWord = new Word(word.getWord());
                    newSentence.add(newWord);
                }
                values2.get(i).add(newSentence);
            }
        }
        return values2;
    }

    public static void main(String[] args) throws IOException {
        File file = new File("src/main/resources/textdata.txt");
        Middleware middleware = new ParagraphMiddleware(file);
        Middleware middleware1 = new SentenceMiddleware(file);
        Middleware middleware2 = new WordMiddleware(file);
        middleware.linkWith(middleware1);
        middleware1.linkWith(middleware2);
        List<CompoundTextElement> values = new ArrayList<CompoundTextElement>();
        SortService sortService = new SortService();
        middleware.getData(values);
        printData(values);
        List<CompoundTextElement> values2 = copyData(values);
        logger.info("AFTER SORTING SENTENCES BY NUMBER OF WORDS:");
        sortService.sortSentencesByNumberOfWords(values);
        printData(values);
        logger.info("AFTER SORTING WORDS BY LENGTH: ");
        sortService.sortWordsByLength(values);
        printData(values);
        logger.info("AFTER SORTING PARAGRAPHS BY NUMBER OF SENTENCES: ");
        sortService.sortParagraphsByNumberOfSentences(values);
        printData(values);
        logger.info("INITIAL ARRAY: ");
        printData(values2);
    }
}
