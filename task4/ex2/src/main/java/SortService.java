import dataModal.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortService {
    public void sortParagraphsByNumberOfSentences(List<CompoundTextElement> values) {
        Collections.sort(values, new Comparator<TextElement>() {
            public int compare(TextElement o1, TextElement o2) {
                Paragraph paragraph = (Paragraph) o1;
                Paragraph paragraph1 = (Paragraph) o2;
                return paragraph.getChildren().size() - paragraph1.getChildren().size();
            }
        });
    }

    public void sortWordsByLength(List<CompoundTextElement> values) {
        for (CompoundTextElement value : values) {
            Paragraph paragraph = (Paragraph) value;
            for (TextElement sentence : paragraph.getChildren()) {
                Sentence sentence1 = (Sentence) sentence;
                Collections.sort(sentence1.getChildren(), new Comparator<TextElement>() {
                    public int compare(TextElement o1, TextElement o2) {
                        Word word1 = (Word) o1;
                        Word word2 = (Word) o2;
                        return word1.getWord().length() - word2.getWord().length();
                    }
                });
            }
        }
    }

    public void sortSentencesByNumberOfWords(List<CompoundTextElement> values) {
        for (CompoundTextElement value : values) {
            Paragraph paragraph = (Paragraph) value;
            Collections.sort(paragraph.getChildren(), new Comparator<TextElement>() {
                public int compare(TextElement o1, TextElement o2) {
                    Sentence sentence = (Sentence) o1;
                    Sentence sentence1 = (Sentence) o2;
                    return sentence.getChildren().size() - sentence1.getChildren().size();
                }
            });
        }
    }
}
