import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StaxParser {
    private static Map<String, String> voucherData;
    private static List<Voucher> voucherList;
    private static String name = "";
    private static XMLEventReader eventReader;
    private static final Logger logger = LogManager.getLogger();

    public List<Voucher> getVouches() throws FileNotFoundException, XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        eventReader = factory.createXMLEventReader(new FileReader("src/main/resources/TouristVoucher.xml"));
        voucherList = new ArrayList<Voucher>();
        voucherData = new HashMap<String, String>();
        retrieveData();
        return voucherList;
    }

    public void retrieveData() throws XMLStreamException {
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    logger.info("Found element:" + qName);
                    name = qName;
                    Iterator<Attribute> attributes = startElement.getAttributes();
                    while (attributes.hasNext()) {
                        Attribute attribute = attributes.next();

                        voucherData.put(attribute.getName().getLocalPart(), attribute.getValue());
                        logger.info("Found attribute:" + attribute.getName().getLocalPart() + " and value:" +
                                attribute.getValue());
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    Characters characters = event.asCharacters();
                    String value = characters.getData().trim();
                    voucherData.put(name, characters.getData().trim());
                    if (!value.equals(""))
                        logger.info("Element name:" + name + " and value:" + characters.getData().trim());
                    name = "";
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("voucher")) {
                        Room.Builder roomBuilder = new Room.Builder().withType(voucherData.get("type"));
                        if (voucherData.containsKey("tv")) roomBuilder.withTV(voucherData.get("tv"));
                        if (voucherData.containsKey("conditioner"))
                            roomBuilder.withConditioner(voucherData.get("conditioner"));
                        Room room = roomBuilder.build();
                        Hotel hotel = new Hotel(Integer.parseInt(voucherData.get("stars")), voucherData.get("meal"), room);
                        Cost cost = new Cost(voucherData.get("amount"), voucherData.get("flight"), voucherData.get("accomodation"));
                        Voucher voucher = new Voucher.Builder()
                                .withId(Integer.parseInt(voucherData.get("id")))
                                .withDays(Integer.parseInt(voucherData.get("days")))
                                .withCountry(voucherData.get("country"))
                                .withTransport(voucherData.get("transport"))
                                .withHotel(hotel)
                                .withCost(cost)
                                .build();
                        voucherList.add(voucher);
                        voucherData.clear();
                    }
                    break;
            }
        }

    }
}
