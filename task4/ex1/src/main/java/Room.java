public class Room {
    private String tv = "NO";
    private String conditioner = "NO";
    private String type;

    public static class Builder {
        private Room room;

        public Builder() {
            room = new Room();
        }

        public Room.Builder withTV(String tv) {
            room.tv = tv;
            return this;
        }

        public Room.Builder withConditioner(String conditioner) {
            room.conditioner = conditioner;
            return this;
        }

        public Room.Builder withType(String type) {
            room.type = type;
            return this;
        }

        public Room build() {
            return room;
        }
    }

    @Override
    public String toString() {
        return "Room{" +
                "tv='" + tv + '\'' +
                ", conditioner='" + conditioner + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
