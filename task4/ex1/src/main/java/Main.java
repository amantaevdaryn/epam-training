import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        DomParser domParser = new DomParser();
        List<Voucher> domVouchers = domParser.getVouchers();
        for (Voucher voucher : domVouchers) {
            System.out.println(voucher);
        }
        SaxParser saxParser = new SaxParser();
        List<Voucher> saxVouchers = saxParser.getVouchers();
        StaxParser staxParser = new StaxParser();
        List<Voucher> staxVouchers = staxParser.getVouches();

        System.out.println("Parsed data by domParser: ");
        for (Voucher voucher : domVouchers) {
            System.out.println(voucher);
        }

        System.out.println("Parsed data by saxParser: ");
        for (Voucher voucher : saxVouchers) {
            System.out.println(voucher);
        }

        System.out.println("Parsed data by staxParser: ");
        for (Voucher voucher : staxVouchers) {
            System.out.println(voucher);
        }


    }
}
