public class Hotel {
    private Integer stars;
    private String meal;
    private Room room;

    public Hotel(Integer stars, String meal, Room room) {
        this.stars = stars;
        this.meal = meal;
        this.room = room;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "stars=" + stars +
                ", meal='" + meal + '\'' +
                ", room=" + room +
                '}';
    }
}
