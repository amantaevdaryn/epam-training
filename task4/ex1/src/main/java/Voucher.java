public class Voucher {
    private Integer id;
    private String country;
    private Integer days;
    private String transport;
    private Hotel hotel;
    private Cost cost;

    public static class Builder {
        private Voucher voucher;

        public Builder() {
            voucher = new Voucher();
        }

        public Builder withId(Integer id) {
            voucher.id = id;
            return this;
        }

        public Builder withCountry(String country) {
            voucher.country = country;
            return this;
        }

        public Builder withDays(Integer days) {
            voucher.days = days;
            return this;
        }

        public Builder withTransport(String transport) {
            voucher.transport = transport;
            return this;
        }

        public Builder withHotel(Hotel hotel) {
            voucher.hotel = hotel;
            return this;
        }

        public Builder withCost(Cost cost) {
            voucher.cost = cost;
            return this;
        }

        public Voucher build() {
            return voucher;
        }
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", days=" + days +
                ", transport='" + transport + '\'' +
                ", hotel=" + hotel +
                ", cost=" + cost +
                '}';
    }
}
