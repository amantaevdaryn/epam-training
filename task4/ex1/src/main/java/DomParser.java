import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DomParser {

    static Map<String, String> voucherData;
    private static final Logger logger = LogManager.getLogger();


    public void retrieveData(NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                String textInformation = node.getNodeValue().replace("\n", "").trim();
                if (!textInformation.isEmpty()) {
                    voucherData.put(node.getParentNode().getNodeName(), node.getNodeValue());
                    System.out.println("Внутри элемента найден текст: " + node.getNodeValue());
                    logger.info("Element name:" + node.getParentNode().getNodeName() +
                            " and value:" + node.getNodeValue());
                }
            } else {
                logger.info("Element name:" + node.getNodeName());

                if (node.getNodeValue() != null) System.out.println(node.getNodeValue());
                NamedNodeMap attributes = node.getAttributes();
                for (int k = 0; k < attributes.getLength(); k++) {
                    logger.info("Found attribute:" + attributes.item(k).getNodeName() + " and value:" +
                            attributes.item(k).getNodeValue());
                    voucherData.put(attributes.item(k).getNodeName(), attributes.item(k).getNodeValue());
                }
            }
            if (node.hasChildNodes())
                retrieveData(node.getChildNodes());
        }
    }

    public List<Voucher> getVouchers() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("src/main/resources/TouristVoucher.xml"));
        NodeList voucherElements = document.getDocumentElement().getElementsByTagName("voucher");
        System.out.println("Всего " + voucherElements.getLength());
        List<Voucher> voucherList = new ArrayList<>();
        for (int i = 0; i < voucherElements.getLength(); i++) {
            voucherData = new HashMap<String, String>();
            NamedNodeMap attributes = voucherElements.item(i).getAttributes();
            for (int k = 0; k < attributes.getLength(); k++) {
                voucherData.put(attributes.item(k).getNodeName(), attributes.item(k).getNodeValue());
            }
            retrieveData(voucherElements.item(i).getChildNodes());
            Room.Builder roomBuilder = new Room.Builder().withType(voucherData.get("type"));
            if (voucherData.containsKey("tv")) roomBuilder.withTV(voucherData.get("tv"));
            if (voucherData.containsKey("conditioner")) roomBuilder.withConditioner(voucherData.get("conditioner"));
            Room room = roomBuilder.build();
            Hotel hotel = new Hotel(Integer.parseInt(voucherData.get("stars")), voucherData.get("meal"), room);
            Cost cost = new Cost(voucherData.get("amount"), voucherData.get("flight"), voucherData.get("accomodation"));
            Voucher voucher = new Voucher.Builder()
                    .withId(Integer.parseInt(voucherData.get("id")))
                    .withDays(Integer.parseInt(voucherData.get("days")))
                    .withCountry(voucherData.get("country"))
                    .withTransport(voucherData.get("transport"))
                    .withHotel(hotel)
                    .withCost(cost)
                    .build();

            voucherList.add(voucher);
        }
        return voucherList;
    }
}
