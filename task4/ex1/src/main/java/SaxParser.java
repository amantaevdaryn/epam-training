import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SaxParser {
    static Map<String, String> voucherData;
    private static List<Voucher> voucherList;
    private static final Logger logger = LogManager.getLogger();

    public List<Voucher> getVouchers() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        SearchingXMLHandler handler = new SearchingXMLHandler("voucher");
        voucherData = new HashMap<String, String>();
        voucherList = new ArrayList<Voucher>();
        parser.parse(new File("src/main/resources/TouristVoucher.xml"), handler);
        return voucherList;
    }

    private static class SearchingXMLHandler extends DefaultHandler {
        private String element;
        private boolean isEntered;
        private String name = "";

        public SearchingXMLHandler(String element) {
            this.element = element;
        }


        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if (qName.equals(element)) {
                isEntered = true;
            }
            if (isEntered) {
                name = qName;
                int length = attributes.getLength();
                for (int i = 0; i < length; i++) {
                    logger.info("Found attribute:" + attributes.getQName(i) + " and value:" +
                            attributes.getValue(i));
                    voucherData.put(attributes.getQName(i), attributes.getValue(i));
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals(element)) {
                isEntered = false;
                Room.Builder roomBuilder = new Room.Builder().withType(voucherData.get("type"));
                if (voucherData.containsKey("tv")) roomBuilder.withTV(voucherData.get("tv"));
                if (voucherData.containsKey("conditioner")) roomBuilder.withConditioner(voucherData.get("conditioner"));
                Room room = roomBuilder.build();
                Hotel hotel = new Hotel(Integer.parseInt(voucherData.get("stars")), voucherData.get("meal"), room);
                Cost cost = new Cost(voucherData.get("amount"), voucherData.get("flight"), voucherData.get("accomodation"));
                Voucher voucher = new Voucher.Builder()
                        .withId(Integer.parseInt(voucherData.get("id")))
                        .withDays(Integer.parseInt(voucherData.get("days")))
                        .withCountry(voucherData.get("country"))
                        .withTransport(voucherData.get("transport"))
                        .withHotel(hotel)
                        .withCost(cost)
                        .build();

                voucherList.add(voucher);
                voucherData.clear();
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (isEntered) {
                String data = new String(ch, start, length).trim();
                voucherData.put(name, data);
                logger.info("Element name:" + name +
                        " and value:" + data);
                name = "";
            }
        }
    }
}
