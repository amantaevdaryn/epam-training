public class Cost {
    private String amount;
    private String flight;
    private String accomodation;

    public Cost(String amount, String flight, String accomodation) {
        this.amount = amount;
        this.flight = flight;
        this.accomodation = accomodation;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "amount='" + amount + '\'' +
                ", flight='" + flight + '\'' +
                ", accomodation='" + accomodation + '\'' +
                '}';
    }
}
