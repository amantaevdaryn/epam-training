<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<h3>Popular in this month</h3>
<div class="row">
    <c:forEach items="${books}" var="item">
        <div class="col-md-4 room">
            <div class="room__image">
                <figure>
                    <img src="${item.imageUrl}" alt="room" width="50%" />
                </figure>
            </div>
            <div class="room__text">
                <h3>${item.name}</h3>
                <a href="book?id=${item.id}">details</a>
            </div>
        </div>
    </c:forEach>
</div>