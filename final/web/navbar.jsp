<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<div class="header-blue">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">

        <a class="navbar-brand" href="/">BookStore</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>


            </ul>
            <c:choose>
                <c:when test="${user != null}">
                    <div>
                        <div class="dropdown">
                            <a class="btn btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ${user.username}
                            </a>
                            <div class="dropdown-menu">
                                <ul>
                                    <li>
                                        <a href="#" class="btn btn-light action-button" role="button">Profile</a>
                                    </li>
                                    <li>
                                        <a href="/logout" class="btn btn-light action-button" role="button">Log out</a>

                                    </li>
                                </ul>


                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                <div>
                      <span class="navbar-text">
                        <a href="/login" class="login-tab">
                          Sign in
                        </a>
                      </span>
                    <a href="/register" class="btn btn-light action-button" role="button">Sign up</a>
                </div>
                </c:otherwise>
            </c:choose>

        </div>
    </nav>
</div>