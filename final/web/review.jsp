<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<div class="row bootstrap snippets">
    <div class="col-md-12 col-md-offset-2 col-sm-12">
        <div class="comment-wrapper">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Comment panel
                </div>
                <div class="panel-body">
                    <c:if test="${user != null}">
                        <div>
                            <form name="form" action=comment method="post">
                                <input type="hidden" name="bookId" value="${book.id}">
                                <div>
                                    <textarea class="form-control" name="comment" placeholder="write a comment..." rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>

                           <hr/>
                        </div>
                    </c:if>
                    <ul class="media-list">

                        <li class="media">
                            <div class="media-body">
                                <span class="text-muted pull-right">
                                    <small class="text-muted">30 min ago</small>
                                </span>
                                <strong class="text-success">@MartinoMont</strong>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Lorem ipsum dolor sit amet, <a href="#">#consecteturadipiscing </a>.
                                </p>
                            </div>
                        </li>


                        <c:forEach items="${reviews}" var="item">
                            <li class="media">
                                <div class="media-body">
                                <span class="text-muted pull-right">
                                    <small class="text-muted">${item.date}</small>
                                </span>
                                    <strong class="text-success">@${item.user.username}</strong>
                                    <p>
                                        ${item.text}
                                    </p>
                                </div>
                            </li>
                        </c:forEach>





                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>