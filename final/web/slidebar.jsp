<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>


<div class="sidebar">
    <h1>${test}</h1>
    <ul>
        <li><a href="/">All</a></li>
        <c:forEach items="${categories}" var="item">
            <li><a href="/?category=${item.name}">${item.name}</a></li>
        </c:forEach>
    </ul>
    <div class="social_media">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
    </div>
</div>
