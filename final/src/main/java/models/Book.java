package models;

public class Book {
    private Integer id;
    private String name;
    private Double price;
    private String author;
    private String description;
    private String imageUrl;
    private Double rating;
    private String sellsInMonth;
    private Integer categoryId;

    public Book() {
    }

    public Book(Integer id, String name, Double price, String author, String description, String imageUrl, Double rating, String sellsInMonth, Integer categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.author = author;
        this.description = description;
        this.imageUrl = imageUrl;
        this.rating = rating;
        this.sellsInMonth = sellsInMonth;
        this.categoryId = categoryId;
    }

    public Book(String name, Double price, String author, String description, String imageUrl, Double rating, String sellsInMonth, Integer categoryId) {
        this.name = name;
        this.price = price;
        this.author = author;
        this.description = description;
        this.imageUrl = imageUrl;
        this.rating = rating;
        this.sellsInMonth = sellsInMonth;
        this.categoryId = categoryId;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Double getRating() {
        return rating;
    }

    public String getSellsInMonth() {
        return sellsInMonth;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", rating=" + rating +
                ", sellsInMonth='" + sellsInMonth + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
