package models;

public class Review {
    private Integer id;
    private String text;
    private String date;
    private Integer userId;
    private Integer bookId;
    private User user;

    public Review(Integer id, String text, String date, Integer userId, Integer bookId) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.userId = userId;
        this.bookId = bookId;
    }

    public Review(String text, String date, Integer userId, Integer bookId, User user) {
        this.text = text;
        this.date = date;
        this.userId = userId;
        this.bookId = bookId;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public Integer getUserId() {
        return userId;
    }


    public Integer getBookId() {
        return bookId;
    }

    public User getUser() {
        return user;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", date='" + date + '\'' +
                ", userId=" + userId +
                ", bookId=" + bookId +
                '}';
    }
}
