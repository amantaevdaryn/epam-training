package DAO;

import com.lambdaworks.crypto.SCryptUtil;
import db.DBConnection;
import models.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.*;
import java.util.List;
import java.util.Optional;

public class UserDAO implements DAO<User> {
    Connection con = null;

    public UserDAO() {
        try {
            con = DBConnection.getInstance();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> get(int id) {
        try {
            Statement st = con.createStatement();
            String query = "select * from user where id='" + id + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next())
                return Optional.ofNullable(new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("email")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<User> getByUsername(String username) {
        try {
            Statement st = con.createStatement();
            String query = "select * from user where username='" + username + "' LIMIT 1";
            ResultSet rs = st.executeQuery(query);
            if (rs.next())
                return Optional.ofNullable(new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getInt("roleId")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public void save(User user) {
        String sql = "INSERT INTO user(username,password,email, roleId) "
                + "VALUES(?,?,?, ?)";
        String hashedPassword = SCryptUtil.scrypt(user.getPassword(), 16, 16, 16);
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, user.getUsername());
            st.setString(2, hashedPassword);
            st.setString(3, user.getEmail());
            st.setInt(4, 2);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(User user, String[] params) {

    }

    @Override
    public void delete(User user) {

    }
}
