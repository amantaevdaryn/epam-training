package DAO;

import db.DBConnection;
import models.Book;
import models.Category;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookDAO implements DAO {

    Connection con = null;

    public BookDAO() {
        try {
            con = DBConnection.getInstance();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> get(int id) {
        Book book = null;
        try {
            Statement st = con.createStatement();
            String query = "select * from book where id='" + id + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                book = new Book(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"), rs.getString("author"),
                        rs.getString("description"),
                        rs.getString("imageUrl"),
                        rs.getDouble("rating"),
                        rs.getString("sellsInMonth"),
                        rs.getInt("categoryId"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(book);
    }

    @Override
    public List<Book> getAll() {
        List<Book> books = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            String query = "select * from book";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                books.add(new Book(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"), rs.getString("author"),
                        rs.getString("description"),
                        rs.getString("imageUrl"),
                        rs.getDouble("rating"),
                        rs.getString("sellsInMonth"),
                        rs.getInt("categoryId")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public void save(Object o) {

    }

    @Override
    public void update(Object o, String[] params) {

    }

    @Override
    public void delete(Object o) {

    }

    public List<Book> search(String category, Integer price) {
        // get category by Id
        List<Book> books = new ArrayList<>();
        try {
            String query = "select * from categories where name='" + category + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            Integer categoryId = 0;
            if (rs.next()) {
                categoryId = rs.getInt("id");
            }
            PreparedStatement filterBooks = con.prepareStatement("SELECT * FROM book WHERE categoryId = ?");
            filterBooks.setInt(1, categoryId);
            rs = filterBooks.executeQuery();
            while (rs.next()) {
                books.add(new Book(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"), rs.getString("author"),
                        rs.getString("description"),
                        rs.getString("imageUrl"),
                        rs.getDouble("rating"),
                        rs.getString("sellsInMonth"),
                        rs.getInt("categoryId")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;

    }
}
