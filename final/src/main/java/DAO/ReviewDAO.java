package DAO;

import com.lambdaworks.crypto.SCryptUtil;
import db.DBConnection;
import models.Book;
import models.Review;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReviewDAO implements DAO<Review> {
    Connection con = null;

    public ReviewDAO() {
        try {
            con = DBConnection.getInstance();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Review> get(int id) {
        return Optional.empty();
    }

    public List<Review> getByBookId(int id) {
        List<Review> review = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            String query = "select * from reviews where bookId='" + id + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                review.add(new Review(rs.getInt("id"), rs.getString("comment"), rs.getString("date1"),
                        rs.getInt("userId"), rs.getInt("bookId")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return review;
    }

    @Override
    public List<Review> getAll() {
        return null;
    }

    @Override
    public void save(Review review) {
        String sql = "INSERT INTO reviews(comment, date1, userId, bookId) "
                + "VALUES(?,?,?, ?)";
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, review.getText());
            st.setString(2, review.getDate());
            st.setInt(3, review.getUserId());
            st.setInt(4, review.getBookId());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Review review, String[] params) {

    }

    @Override
    public void delete(Review review) {

    }
}
