package DAO;

import db.DBConnection;
import models.Category;
import models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryDAO implements DAO {
    Connection con = null;

    public CategoryDAO() {
        try {
            con = DBConnection.getInstance();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Optional get(int id) {
        return Optional.empty();
    }

    @Override
    public List<Category> getAll() {
        List<Category> categories = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            String query = "select * from categories";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                categories.add(new Category(rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    @Override
    public void save(Object o) {

    }

    @Override
    public void update(Object o, String[] params) {

    }

    @Override
    public void delete(Object o) {

    }
}
