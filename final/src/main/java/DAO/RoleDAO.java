package DAO;

import db.DBConnection;
import models.Role;
import models.RoleEnum;
import models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class RoleDAO implements DAO<Role> {
    Connection con = null;

    public RoleDAO() {
        try {
            con = DBConnection.getInstance();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Role> get(int id) {
        try {
            Statement st = con.createStatement();
            String query = "select * from roles where id='" + id + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                return Optional.of(new Role(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Role> getAll() {
        return null;
    }

    @Override
    public void save(Role role) {

    }

    @Override
    public void update(Role role, String[] params) {

    }

    @Override
    public void delete(Role role) {

    }
}
