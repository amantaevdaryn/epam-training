INSERT INTO categories(id, name)
VALUES (NULL, 'Fiction'),
       (NULL, 'Science'),
       (NULL, 'Mystery'),
       (NULL, 'Horror'),
       (NULL, 'History'),
       (NULL, 'Tech'),
       (NULL, 'Biographies');


INSERT INTO book(id, name, price, author, description, imageUrl, rating, sellsInMonth, categoryId)
VALUES (NULL, 'In Search of Lost Tim',  150, 'Marcel Proust', 'Swann''s Way, the first part of A la recherche de temps perdu,
        Marcel Proust''s seven-part cycle, was published in 1913. In it, Proust introduces the themes that run through the entire work.
        The narrator recalls his childhood, aided by the famous madeleine; and describes M. Swann''s passion for Odette. The work is incomparable',
        'https://m.media-amazon.com/images/I/51tRkYYlpaL._SY346_.jpg',
        88, 350, 1),
       (NULL, 'Ulysses', 200,'James Joyce', 'Ulysses chronicles the passage of Leopold Bloom through Dublin during an ordinary day, June 16, 1904.
         The title parallels and alludes to Odysseus (Latinised into Ulysses), the hero of Homer''s Odyssey
        (e.g., the correspondences between Leopold Bloom and Odysseus, Molly Bloom and Penelope, and Stephen Dedalus and Telemachus).
        Joyce fans worldwide now celebrate June 16 as Bloomsday.',
        'https://images-na.ssl-images-amazon.com/images/I/41i+rML5JsL._SX324_BO1,204,203,200_.jpg',
        70, 250,  3),
       (NULL, 'Don Quixote', 300, 'Miguel de Cervantes', 'Alonso Quixano, a retired country gentleman in his fifties,
        lives in an unnamed section of La Mancha with his niece and a housekeeper. He has become obsessed with books of
        chivalry, and believes their every word to be true, despite the fact that
        many of the events in them are clearly impossible. Quixano eventually appears to other people to have
        lost his mind from little sleep and food and because of so much reading.',
        'https://img1.wbstatic.net/big/new/7400000/7408129-1.jpg',
        86, 500,  5),
       (NULL, 'The Great Gatsby', 200, 'F. Scott Fitzgerald', 'The novel chronicles an era that Fitzgerald himself dubbed
        the "Jazz Age". Following the shock and chaos of World War I, American society enjoyed unprecedented levels
        of prosperity during the "roaring" 1920s as the economy soared. At the same time, Prohibition,
        the ban on the sale and manufacture of alcohol as mandated by the Eighteenth Amendment, made millionaires
        out of bootleggers and led to an increase in organized crime, for example the Jewish mafia. Although Fitzgerald,
        like Nick Carraway in his novel, idolized the riches and glamor of the age, he was uncomfortable with the
        unrestrained materialism and the lack of morality that went with it, a kind of decadence.',
        'https://images-na.ssl-images-amazon.com/images/I/41iers+HLSL._SX326_BO1,204,203,200_.jpg',
        90, 363,  2),
       (NULL, 'One Hundred Years of Solitude', 148,'Gabriel Garcia Marquez', 'One of the 20th century''s enduring works,
        One Hundred Years of Solitude is a widely beloved and acclaimed novel known throughout the world, and the
        ultimate achievement in a Nobel Prize–winning career. The novel tells the story of the rise and fall of the
        mythical town of Macondo through the history of the Buendía family. It is a rich and brilliant chronicle of
        life and death, and the tragicomedy of humankind. In the noble, ridiculous, beautiful, and tawdry story of the
        Buendía family, one sees all of humanity, just as in the history, myths, growth, and decay of Macondo, one sees
        all of Latin America. Love and lust, war and revolution, riches and poverty, youth and senility — the variety of
        life, the endlessness of death, the search for peace and truth — these universal themes dominate the novel.
        Whether he is describing an affair of passion or the voracity of capitalism and the corruption of government,
        Gabriel García Márquez always writes with the simplicity, ease, andpurity that are the mark of a master.',
        'https://images-na.ssl-images-amazon.com/images/I/81MI6+TpYkL.jpg',
        83, 250,  4);


INSERT INTO roles(id, name)
VALUES (1, 'ADMIN'),
       (2, 'USER');

INSERT INTO reviews(id, comment, date1, userId, bookId)
VALUES (1, 'test comment', '2020-05-23', 1, 11);



