package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static Connection instance;
    private static String url = "jdbc:mysql://localhost:3306/book_shop?serverTimezone=UTC";
    private static String dbUsername = "daryn";
    private static String dbPassword = "qwerty12345";
    public static Connection getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            Class.forName("com.mysql.jdbc.Driver");
            instance = DriverManager.getConnection(url, dbUsername, dbPassword);
        }
        return instance;
    }
}
