CREATE DATABASE IF NOT EXISTS book_shop;
USE book_shop;

CREATE TABLE IF NOT EXISTS Roles
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS User
(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    password VARCHAR(255),
    email VARCHAR(255),
    roleId INT,
    PRIMARY KEY (id),
    CONSTRAINT Fk_UserRoles FOREIGN KEY (roleId)
    REFERENCES Roles(id)
);

CREATE TABLE IF NOT EXISTS Categories
(
    id   INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Book(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    price DOUBLE(10, 2),
    author VARCHAR(255),
    description TEXT,
    imageUrl VARCHAR(255),
    rating DOUBLE,
    sellsInMonth INT,
    categoryId INT,
    PRIMARY KEY (id),
    CONSTRAINT FK_BookCategory FOREIGN KEY (categoryId)
    REFERENCES Categories(id)
);

CREATE TABLE IF NOT EXISTS Purchases(
    id INT NOT NULL AUTO_INCREMENT,
    bookId INT NOT NULL,
    userId INT NOT NULL,
    purchase_time DATETIME,
    CONSTRAINT FK_PurchaseBook FOREIGN KEY (bookId)
    REFERENCES Book(id),
    CONSTRAINT FK_PurchaseUser FOREIGN KEY (userId)
    REFERENCES User(id)
);

CREATE TABLE IF NOT EXISTS Reviews(
    id INT NOT NULL AUTO_INCREMENT,
    comment TEXT,
    date1 DATETIME,
    userId INT NOT NULL,
    bookId INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT FK_ReviewUser FOREIGN KEY (userId)
    REFERENCES User(id),
    CONSTRAINT FK_ReviewBook FOREIGN KEY (bookId)
    REFERENCES Book(id)
)



