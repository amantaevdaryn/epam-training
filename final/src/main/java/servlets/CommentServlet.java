package servlets;

import DAO.ReviewDAO;
import DAO.UserDAO;
import models.Review;
import models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {

    UserDAO userDAO;
    ReviewDAO reviewDAO;

    public CommentServlet() {
        userDAO = new UserDAO();
        reviewDAO = new ReviewDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String comment = req.getParameter("comment");
        Integer bookId = Integer.parseInt(req.getParameter("bookId"));
        User user = (User) session.getAttribute("user");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Review review = new Review(comment, formatter.format(new Date()), user.getId(), bookId, user);
        reviewDAO.save(review);
        resp.sendRedirect("/book?id=" + bookId);

    }
}