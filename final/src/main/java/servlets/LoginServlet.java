package servlets;

import DAO.RoleDAO;
import DAO.UserDAO;
import com.lambdaworks.crypto.SCryptUtil;
import db.DBConnection;
import models.Role;
import models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Optional;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    UserDAO userDAO;
    RoleDAO roleDAO;

    public void init() {
        userDAO = new UserDAO();
        roleDAO = new RoleDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        req.setAttribute("username", username);
        Optional<User> optionalUser = userDAO.getByUsername(username);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            boolean matched = SCryptUtil.check(password, user.getPassword());
            if (matched) {
                HttpSession session = req.getSession();
                Role role = roleDAO.get(user.getRoleId()).get();
                user.setRole(role);
                session.setAttribute("user", user);
                resp.sendRedirect("/");
            }
        }
        else resp.sendRedirect("login.jsp?");
    }


}
