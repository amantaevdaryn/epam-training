package servlets;

import DAO.BookDAO;
import DAO.CategoryDAO;
import models.Book;
import models.Category;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/q")
public class SearchServlet extends HttpServlet {
    CategoryDAO categoryDAO;
    BookDAO bookDAO;

    public void init() {
        categoryDAO = new CategoryDAO();
        bookDAO = new BookDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = null;
        Integer price = null;
        if (req.getParameterMap().containsKey("category")) {
            id = req.getParameter("category");
        }
        if (req.getParameterMap().containsKey("price")) {
            price = Integer.parseInt(req.getParameter("price"));
        }
        List<Category> categories = categoryDAO.getAll();
        req.getRequestDispatcher("index.jsp?").forward(req, resp);
    }

}
