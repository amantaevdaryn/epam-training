package servlets;

import DAO.BookDAO;
import DAO.CategoryDAO;
import DAO.ReviewDAO;
import DAO.UserDAO;
import models.Book;
import models.Category;
import models.Review;
import models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/book")
public class BookServlet extends HttpServlet {
    CategoryDAO categoryDAO;
    BookDAO bookDAO;
    ReviewDAO reviewDAO;
    UserDAO userDAO;

    public void init() {
        categoryDAO = new CategoryDAO();
        bookDAO = new BookDAO();
        reviewDAO = new ReviewDAO();
        userDAO = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = (String) req.getParameter("id");
        List<Category> categories = categoryDAO.getAll();
        Book book = bookDAO.get(Integer.parseInt(id)).orElse(new Book());
        List<Review> reviews = reviewDAO.getByBookId(book.getId());
        // get user by id
        for (Review review : reviews) {
            Optional<User> optUser = userDAO.get(review.getUserId());
            if (optUser.isPresent()) {
                review.setUser(optUser.get());

            }
        }
        req.setAttribute("book", book);
        req.setAttribute("categories", categories);
        req.setAttribute("reviews", reviews);
        req.getRequestDispatcher("book.jsp?").forward(req, resp);
    }

}
