package servlets;

import DAO.BookDAO;
import DAO.CategoryDAO;
import DAO.UserDAO;
import models.Book;
import models.Category;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("")
public class HomeServlet extends HttpServlet {
    CategoryDAO categoryDAO;
    BookDAO bookDAO;

    public void init() {
        categoryDAO = new CategoryDAO();
        bookDAO = new BookDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Category> categories = categoryDAO.getAll();
        List<Book> books = bookDAO.getAll();
        req.setAttribute("categories", categories);
        if (req.getParameterMap().containsKey("category")) {
            books = getBooks(req);
        }
        req.setAttribute("books", books);
        HttpSession session = req.getSession();
        req.getRequestDispatcher("index.jsp?").include(req, resp);
    }

    private List<Book> getBooks(HttpServletRequest req) {
        String category = null;
        Integer price = null;
        if (req.getParameterMap().containsKey("category")) {
            category = req.getParameter("category");
        }
        if (req.getParameterMap().containsKey("price")) {
            price = Integer.parseInt(req.getParameter("price"));
        }
        return bookDAO.search(category, 0);

    }
}
