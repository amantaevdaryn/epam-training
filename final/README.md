# BOOK STORE

Book store web platform

## Installation
 1. Import project
 2. Use [maven](https://maven.apache.org/) to build the project
    ```bash
    mvn install
    ```
 3. Download and configure the web server [tomcat](http://tomcat.apache.org/) 
