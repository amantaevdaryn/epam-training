import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class MatrixManipulation {
    private int a[][];
    private int matrix_size;
    ReentrantLock lock = new ReentrantLock();
    private static final Logger logger = LogManager.getLogger();

    public MatrixManipulation(int matrix_size) {
        this.matrix_size = matrix_size;
        a = new int[matrix_size][matrix_size];
        Random ran = new Random();
        for (int i = 0; i < matrix_size; i++) {
            for (int j = 0; j < matrix_size; j++) {
                if (i != j) a[i][j] = ran.nextInt(8);
            }
        }
    }

    public void doChange(String info, int val2, int i) {
        try {
            lock.lock();
            if (a[i][i] == 0) {
                a[i][i] = val2;
                logger.info(info + " changed cell # " + i);
            } else {
                logger.error(info + " cannot change the cell # " + i);
            }

            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(500));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public int getMatrix_size() {
        return matrix_size;
    }

    public int[][] getMatrix() {
        return a;
    }
}
