import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class MatrixThread extends Thread {
    private MatrixManipulation matrixManipulation;
    private int val;

    public MatrixThread(String name, int val, MatrixManipulation matrixManipulation) {
        super(name);
        this.val = val;
        this.matrixManipulation = matrixManipulation;
    }

    public void run() {
        for (int i = 0; i < matrixManipulation.getMatrix_size(); i++) {
            matrixManipulation.doChange(this.getName(), val, i);
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
