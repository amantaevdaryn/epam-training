import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        int n = 0, m = 0;
        try (FileReader reader = new FileReader("src/main/resources/data.txt");
             BufferedReader br = new BufferedReader(reader)) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (n != 0) m = Integer.parseInt(line);
                    else n = Integer.parseInt(line);
                }
                MatrixManipulation matrixManipulation = new MatrixManipulation(n);
                String names[] = {"First", "Second", "Third", "Forth", "Fifth", "Sixth"};
                for (int i = 0; i < m; i++) {
                    new MatrixThread(names[i], i + 1, matrixManipulation).start();
                }
                try {
                    TimeUnit.SECONDS.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int a[][] = matrixManipulation.getMatrix();
                for (int i = 0; i < a.length; i ++) {
                    for (int j = 0; j < a.length; j ++) {
                        System.out.print(a[i][j] + " ");
                    }
                    System.out.println();
                }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }
}
