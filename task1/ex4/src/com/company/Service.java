package com.company;

import java.util.*;

public class Service {
    public int[][] sortByCoumn(int a[][], int k) {
        Arrays.sort(a, new Comparator<int[]>() {
            @Override
            public int compare(int[] a1, int[] b1) {
                if (a1[k] > b1[k]) return 1;
                else return -1;
            }
        });
        return a;
    }
    public int[][] sortByRow(int a[][], int k) {
        List<Pair> p = new ArrayList<Pair>();
        for (int i = 0; i < a.length; i ++) {
            p.add(new Pair(a[k][i], i));
        }
        Collections.sort(p);
        int temp[][] = new int[a.length][a.length];
        for (int  i = 0; i < p.size(); i ++) {
            int x = p.get(i).getIndx();
            for (int j = 0; j < a.length; j++) {
                temp[j][i] = a[j][x];
            }
        }
        return temp;
    }

    public void printData(int a[][]) {
        for (int i = 0; i < a.length; i ++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
    public void printSumBetweenPositives(int a[][]) {
        for (int i = 0; i < a.length; i ++) {
            int sum = 0;
            boolean f = false;
            int fi = -1, se = -1;
            for (int j = 0; j < a.length; j ++) {
                if (a[i][j] > 0){
                    if (!f) {
                        f = true;
                        fi = a[i][j];
                    }
                    else {
                        se = a[i][j];
                        break;
                    }
                }
                else if (f) {
                    sum += a[i][j];
                }
            }
            if (fi != -1 && se != -1) {
                System.out.println(i + 1 + "." + " 2 positive elements not found");
            }
            else
            System.out.println(i + 1 + "." + " Elements:" +  fi + "," + se + ";" + "Sum: " + sum);
        }
    }

    class Pair implements Comparable<Pair> {
        int v;
        int indx;

        public Pair(int v, int indx) {
            this.v = v;
            this.indx = indx;
        }

        public int getV() {
            return v;
        }

        public int getIndx() {
            return indx;
        }

        @Override
        public int compareTo(Pair o) {
            return this.getV() - o.getV();
        }
    }
}
