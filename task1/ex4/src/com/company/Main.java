package com.company;

public class Main {


    static Service service = new Service();
    public static void main(String[] args) {
        ReadBuffer readBuffer = new ReadBuffer();
        try {
            readBuffer.read();
        } catch (Exception e) {
            return;
        }

        int a[][] = readBuffer.getA();
        int k = readBuffer.getK();
        int order = readBuffer.getOrder();
        System.out.println("Before sort:");
        service.printData(a);
        System.out.println("After sort:");
        if (order == 1)
            a = service.sortByCoumn(a, k);
        else a = service.sortByRow(a, k);
        service.printData(a);
        service.printSumBetweenPositives(a);

    }
}
