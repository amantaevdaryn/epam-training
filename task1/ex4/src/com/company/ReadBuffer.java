package com.company;

import java.util.*;

public class ReadBuffer {

    static Scanner sc = new Scanner(System.in);
    static Random rand = new Random();
    int n;
    int a[][];
    int order;
    int k;


    public  void read() throws Exception{
        try {
            System.out.println("Please enter the size of matrix  (n x n):");
            n = sc.nextInt();
            a = new int[n][n];
            System.out.println("Please enter the number that would fulfill matrix with data in range [-number...number] ");
            int m = sc.nextInt();
            System.out.println("Order: \n [1] rows by column\n [2] columns by row");
            order = sc.nextInt();

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    a[i][j] = rand.nextInt(2 * m + 1) - m;
                }
            }
            System.out.println("Please enter " + ((order == 1) ? "column" : "row") + " as value from 1 " + "to " + n);
            k = sc.nextInt();
            k--;
            validateData();
        }
        catch (InputMismatchException e) {
            System.out.println("Error in entered data");
            throw e;
        }
    }
    public void validateData() {
        if (k < 0 || k >= n || order < 1 || order > 2)
        throw new InputMismatchException();
    }

    public int[][] getA() {
        return a;
    }

    public int getOrder() {
        return order;
    }

    public int getK() {
        return k;
    }
}
