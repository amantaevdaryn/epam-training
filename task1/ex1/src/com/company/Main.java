package com.company;

import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Hello. Enter your name, please");
        String name = sc.nextLine();
        System.out.println("Hello, " + name + "!!!");
        System.out.println(name +  ", please enter space separated integer values");
        String str = sc.nextLine();
        Scanner strScan = new Scanner(str);
        int sum = 0;
        int product = 1;
        while(strScan.hasNextInt()) {
            int x = strScan.nextInt();
            sum += x;
            product *= x;
        }
        System.out.println("Sum of entered numbers is " + sum + ". Product is " + product);
    }
}
