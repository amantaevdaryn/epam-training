package com.company;

import java.util.HashMap;

public class MonthYearManipulation {
    int monthNumber = 0;
    int yearNumber = 0;
    HashMap<Integer, String> nameOfMonths = new HashMap<>() {{
        put(1, "January");
        put(2, "February");
        put(3, "March");
        put(4, "April");
        put(5, "May");
        put(6, "June");
        put(7, "July");
        put(8, "August");
        put(9, "September");
        put(10, "October");
        put(11, "November");
        put(12, "December");

    }};
    public MonthYearManipulation(int yearNumber, int monthNumber) {
        this.yearNumber = yearNumber;
        this.monthNumber = monthNumber;
    }
    public String getPrintMonthName() {
        String nameOfMonth = nameOfMonths.get(monthNumber);
        return nameOfMonth;
    }
    public boolean isYearLeap() {
        return (((yearNumber % 4 == 0) && (yearNumber % 100 != 0)) || (yearNumber % 400 == 0));
    }
    public void printAll() {
        String nameOfMonth = getPrintMonthName();
        String leap = isYearLeap() ? "leap year" : "not leap year";
        System.out.println(yearNumber + " " + nameOfMonth + " - " + leap);
    }
}
