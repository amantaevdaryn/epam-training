package com.company;

public class Main {

    public static void main(String[] args) {
        ReadBuffer.read();
        if (ReadBuffer.isValid()) {
            int year = ReadBuffer.getYear();
            int month = ReadBuffer.getMonth();
            MonthYearManipulation monthYearManipulation = new MonthYearManipulation(year, month);
            monthYearManipulation.printAll();
        }
    }
}
