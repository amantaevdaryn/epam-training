package com.company;

import java.util.Scanner;

public class ReadBuffer {
    static Scanner sc = new Scanner(System.in);
    static int year;
    static int month;
    static boolean valid = true;
    public static void read() {
        System.out.println("Please enter the year");
        year = sc.nextInt();
        System.out.println("Please enter the month");
        month = sc.nextInt();
        if (year < 1 || month < 1) {
            System.out.println("The year and month cannot be negative or equal to zero");
            valid = false;
        }
        if (month > 12) {
            System.out.println("Moth have to lie in range [1..12]");
            valid = false;
        }
    }
    public static int getYear() {
        return year;
    }

    public static int getMonth() {
        return month;
    }

    public static boolean isValid() {
        return valid;
    }
}
