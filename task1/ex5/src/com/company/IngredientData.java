package com.company;

import java.util.ArrayList;

public class IngredientData {
    public static ArrayList<Ingredient> ingredients = new ArrayList<>()
    {
        {
            add(new Ingredient("Tomato Paste", 1));
            add(new Ingredient("Cheese", 1));
            add(new Ingredient("Salami", 1.5));
            add(new Ingredient("Bacon", 1.2));
            add(new Ingredient("Garlic", 0.3));
            add(new Ingredient("Corn", 0.7));
            add(new Ingredient("Pepperoni", 1.5));
            add(new Ingredient("Pepper", 0.6));
            add(new Ingredient("Olives", 0.5));
        }
    };
    public static Ingredient getIngredientByName(String name){
        for (Ingredient ingredient: ingredients) {
            if (ingredient.getName() == name) {
                return ingredient;
            }
        }
        return null;
    }
}
