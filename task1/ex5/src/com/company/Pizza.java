package com.company;

import java.util.ArrayList;

public class Pizza {
    private String name;
    private ArrayList<Ingredient> ingredients;
    private PizzaType pizzaType;
    private int number;
    private double basePrice;
    private double total;

    public Pizza(String name, ArrayList<Ingredient> ingredients, PizzaType pizzaType, int number) {
        this.name = name;
        this.ingredients = ingredients;
        this.pizzaType = pizzaType;
        this.number = number;
        if (pizzaType == PizzaType.CALZONE) this.basePrice = 1.5;
        else this.basePrice = 1;
    }

    public Pizza(String name, PizzaType pizzaType, int number) {
        this.name = name;
        this.pizzaType = pizzaType;
        this.number = number;
        this.ingredients = new ArrayList<>();
        if (pizzaType == PizzaType.CALZONE) this.basePrice = 1.5;
        else this.basePrice = 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public double getTotalPrice() {
        double total = getBasePrice();
        for (Ingredient ingredient : ingredients) {
            total += ingredient.getPrice();
        }
        return total;
    }

    public void addIngredient(Ingredient ingredient) {
        if (ingredient == null || ingredients.size() >= IngredientData.ingredients.size()) return;
        for (Ingredient i : ingredients) {
            if (i.getName() == ingredient.getName()) return;
        }
        ingredients.add(ingredient);

    }

    @Override
    public String toString() {

        int leftPad = 19 - (pizzaType == PizzaType.CALZONE ? 9 : 0);

        String result =
                "Название: " + name + "\n" +
                "-------------------------------- \n" +
                "Pizza Base " + (pizzaType == PizzaType.CALZONE ? "(Calzone)" : "") +
                        String.format("%" + leftPad + ".2f €", getBasePrice()) + '\n';

        for (Ingredient pizzaIngredient : getIngredients()) {
            result += pizzaIngredient;
        }
        result += "-------------------------------- \n";
        result += "Всего" + "           " +String.format("%14.2f €\n",getTotalPrice());
        result += "Кол-во: " +  String.format("%24d\n", number);
        result += "-------------------------------- \n";
        return result;
    }
}
