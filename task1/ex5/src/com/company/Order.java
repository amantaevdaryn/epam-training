package com.company;

import java.util.ArrayList;

public class Order {
    private static int orderId = 10000;

    private int clientId;

    ArrayList<Pizza> pizzaList;

    public Order(int clientId, ArrayList<Pizza> pizzaList) throws Exception {
        this.orderId += 1;
        this.clientId = clientId;
        this.pizzaList = pizzaList;
        validateData();
    }
    public double getFinalPrice(){
        double total = 0;
        for (Pizza pizza: pizzaList) {
            total += pizza.getNumber() * pizza.getTotalPrice();
        }
        return total;
    }
    public void validateData() throws Exception {
        int numOfPizza = 1;
        for (Pizza pizza : pizzaList) {
            String pizzaName = pizza.getName();
            if (pizzaName.length() < 4 || pizzaName.length() > 20) {
                String newName = clientId + "_" + numOfPizza;
                pizza.setName(newName);
            }
        }
    }
    public void printPizzaAttributes() {
        for (Pizza pizza: getPizzaList()) {
            String val = orderId + ":" + clientId + ":" + pizza.getName() + ":" + pizza.getNumber();
            String t = String.format("[%s]", val);
            System.out.println(t);
        }
    }

    public static int getOrderId() {
        return orderId;
    }

    public static void setOrderId(int orderId) {
        Order.orderId = orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public ArrayList<Pizza> getPizzaList() {
        return pizzaList;
    }

    public void setPizzaList(ArrayList<Pizza> pizzaList) {
        this.pizzaList = pizzaList;
    }

    @Override
    public String toString() {
        String result =
                "******************************** \n"+
                "Заказ: " + orderId + "\n" +
                "Клиент: " + clientId + "\n";
        for (Pizza pizza: pizzaList) {
            result += pizza.toString();

        }
        result += "Общая сумма: " + String.format("%17.2f €", getFinalPrice());
        return result;
    }
}
