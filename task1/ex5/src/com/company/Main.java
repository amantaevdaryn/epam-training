package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {

        ArrayList<Pizza> pizzas = new ArrayList<>();

        Pizza pizza = new Pizza("Margarita", PizzaType.CALZONE, 2);

        pizza.addIngredient(IngredientData.getIngredientByName("Tomato Paste"));
        pizza.addIngredient(IngredientData.getIngredientByName("Pepper"));
        pizza.addIngredient(IngredientData.getIngredientByName("Garlic"));
        pizza.addIngredient(IngredientData.getIngredientByName("Bacon"));


        Pizza pizza2 = new Pizza("Pepperoni", PizzaType.REGULAR, 3);
        pizza2.addIngredient(IngredientData.getIngredientByName("Tomato Paste"));
        pizza2.addIngredient(IngredientData.getIngredientByName("Cheese"));
        pizza2.addIngredient(IngredientData.getIngredientByName("Pepperoni"));
        pizza2.addIngredient(IngredientData.getIngredientByName("Olives"));

        pizzas.add(pizza);
        pizzas.add(pizza2);

        Order order = new Order(7717, pizzas);

        System.out.println(order);

    }
}
