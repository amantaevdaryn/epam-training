package com.company;

public class Ingredient {
    private String name;
    private double price;

    public Ingredient(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        String numOfSpaces = "";
        int numOfDigitsInPrice = String.valueOf(price).length();
        int leftPad = 30  - name.length();
        String result =
                 name + String.format("%" + leftPad + ".2f", price) + " €\n";
        return result;
    }
}
