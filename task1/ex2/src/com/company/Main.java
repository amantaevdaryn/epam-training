package com.company;


public class Main {

    public static void main(String[] args) {
        ReadBuffer.read();
        int []arr = ReadBuffer.getArr();
        DigitsManipulation digitsManipulation = new DigitsManipulation(arr);
        digitsManipulation.printAll();
    }
}
