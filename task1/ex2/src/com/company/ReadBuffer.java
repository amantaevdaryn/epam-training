package com.company;

import java.util.Scanner;

public class ReadBuffer {

    public static int arr[];

    public static void read() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter the number of integer values");
        int n = sc.nextInt();
        System.out.println("Please, enter " + n + " space separated integer values");
        arr = new int[n];
        for (int i = 0; i < n; i ++) {
            int value = sc.nextInt();
            arr[i] = value;
        }
    }

    public static int[] getArr(){
        return arr;
    }
}
