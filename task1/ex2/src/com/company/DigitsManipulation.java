package com.company;

public class DigitsManipulation {
    int n = 0;
    int arr[];

    public DigitsManipulation(int arr[]) {
        this.arr = arr;
        n = arr.length;
    }

    public int numberOfDigits(int n) {
        int k = 0;
        while (n > 0){
            n /= 10;
            k ++;
        }
        return k;
    }
    public boolean isDistinctDigits(int n) {
        boolean visited[] = new boolean[10];
        while (n > 0) {
            if (visited[n % 10]) return false;
            visited[n % 10] = true;
            n /= 10;
        }
        return true;
    }
    public boolean isInAscendingOrder(int n) {
        int last = 10;
        while(n > 0) {
            if (n % 10 > last) return false;
            last = n % 10;
            n /= 10;
        }
        return true;
    }

    public void printShortestInteger() {
        int shortestInt = 0, minLength = Integer.MAX_VALUE;
        for (int i = 0; i  < n; i ++) {
            int digitsNum = numberOfDigits(arr[i]);
            if (digitsNum < minLength) {
                minLength = digitsNum;
                shortestInt = arr[i];
            }
        }
        System.out.println("Shortest integer is " + shortestInt + " with length " + minLength);
    }

    public void printLongestInteger() {
        int longestInt = 0, maxLength = Integer.MIN_VALUE;
        for (int i = 0; i  < n; i ++) {
            int digitsNum = numberOfDigits(arr[i]);
            if (digitsNum > maxLength) {
                maxLength = digitsNum;
                longestInt = arr[i];
            }
        }
        System.out.println("Longest integer is " + longestInt + " with length " + maxLength);
    }
    public void printFirstDistincNumber() {
        for (int i = 0; i < n; i ++) {
            if (isDistinctDigits(arr[i])) {
                System.out.println("The first number with distinct digits is " + arr[i]);
                return;
            }
        }
        System.out.println("There is no number with distinct digits");
    }
    public void printFirstSortedNumber() {
        for (int i = 0; i < n; i ++) {
            if (isInAscendingOrder((arr[i]))) {
                System.out.println("The first number sorted in ascending digits is " + arr[i]);
                return;
            }
        }
        System.out.println("There is no number with sorted in ascending order digits");
    }
    public void printAll() {
        printShortestInteger();
        printLongestInteger();
        printFirstDistincNumber();
        printFirstSortedNumber();
    }
}
