package com.company;

public abstract class GemStone implements Comparable<GemStone> {
    private double price;
    private double weight;
    private double transparency;
    private boolean isPrecious;

    public GemStone(double price, double weight, double transparency, boolean isPrecious) {
        this.price = price;
        this.weight = weight;
        this.transparency = transparency;
        this.isPrecious = isPrecious;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getTransparency() {
        return transparency;
    }

    public void setTransparency(double transparency) {
        this.transparency = transparency;
    }

    public boolean isPrecious() {
        return isPrecious;
    }

    public void setPrecious(boolean precious) {
        isPrecious = precious;
    }

    @Override
    public int compareTo(GemStone o) {
        if (this.getClass().equals(o.getClass())) return 0;
        return Double.compare(this.price, o.getPrice());

    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                " price=" + price +
                ", weight=" + weight +
                ", transparency=" + transparency +
                ", isPrecious=" + isPrecious +
                "}\n";
    }
}
