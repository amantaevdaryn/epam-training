package com.company;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        Set<GemStone> gemStoneList = new TreeSet<>();
        Diamond diamond = new Diamond(15000, 1, 0.6, true);
        Grandidierite grandidierite = new Grandidierite(20000, 1, 0.3, true);
        Garnet garnet = new Garnet(7500, 1, 0.5, false);
        gemStoneList.add(garnet);
        gemStoneList.add(diamond);
        gemStoneList.add(grandidierite);
        Jewelry jewelry = new Jewelry(gemStoneList);
        System.out.println(jewelry);
        System.out.println("Enter the left bound of transparency: ");
        double l = sc.nextDouble();
        System.out.println("Enter the right bound of transparency: ");
        double r = sc.nextDouble();
        System.out.println("Gems with specified bounds: ");
        for (GemStone gemStone: jewelry.getGemStoneList()) {
            System.out.print(gemStone);
        }

    }
}
