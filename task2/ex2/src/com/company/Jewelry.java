package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Jewelry {
    private Set<GemStone> gemStoneList;
    private double totalWeight;
    private double totalPrice;

    public Jewelry(Set<GemStone> gemStoneList) {
        this.gemStoneList = gemStoneList;
        for (GemStone gemStone : gemStoneList) {
            totalWeight += gemStone.getWeight();
            totalPrice += (gemStone.getWeight() * gemStone.getPrice());
        }
    }

    public Set<GemStone> getGemStoneList() {
        return gemStoneList;
    }

    public void setGemStoneList(Set<GemStone> gemStoneList) {
        this.gemStoneList = gemStoneList;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<GemStone> findByTransparencyInRange(double l, double r) {
        List<GemStone> filteredGemStones = new ArrayList<>();
        for (GemStone gemStone : gemStoneList) {
            if (gemStone.getTransparency() >= l && gemStone.getTransparency() <= r)
                filteredGemStones.add(gemStone);
        }
        return filteredGemStones;
    }

    @Override
    public String toString() {
        String s = "";
        for (GemStone gem : gemStoneList) {
            s += gem;
        }
        return s;
    }
}
