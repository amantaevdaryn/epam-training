package com.company;

public abstract class Candy implements Comparable<Candy> {
    private double weight;
    private double price;
    private double sugar_amount;

    public Candy(double weight, double price, double sugar_amount) {
        this.weight = weight;
        this.price = price;
        this.sugar_amount = sugar_amount;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSugar_amount() {
        return sugar_amount;
    }

    public void setSugar_amount(double sugar_amount) {
        this.sugar_amount = sugar_amount;
    }

    @Override
    public int compareTo(Candy o) {
        if (this.getClass().equals(o.getClass())) return 0;
        return Double.compare(this.weight, o.getWeight());
    }


    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "weight=" + weight +
                ", price=" + price +
                ", sugar_amount=" + sugar_amount +
                '}' + '\n';
    }
}
