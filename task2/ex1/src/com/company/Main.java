package com.company;

import java.util.*;

public class Main {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Set<Candy> candies = new TreeSet<>();
        candies.add(new Snickers(54.1, 12, 12));
        candies.add(new Snickers(54.3, 12, 13));
        candies.add(new Lollipop(15.5, 12, 14));
        candies.add(new Nesquik(10.3, 123, 15));
        candies.add(new Snickers(54.1, 12, 16));
        Present present = new Present(candies);
        System.out.println("Sorted present by weight: ");
        System.out.println(present.toString());
        System.out.println("Enter the left bound of sugar amount in candy: ");
        double l = sc.nextDouble();
        System.out.println("Enter the right bound of sugar amount in candy: ");
        double r = sc.nextDouble();
        System.out.println("Candies with specified bounds: ");
        for (Candy candy : present.getCandiesBySugarBounds(l, r)) {
            System.out.print(candy);
        }
    }
}
