package com.company;

import java.util.*;

public class Present {
    Set<Candy> candies;
    private double weight;
    private double total_sum;

    public Present() {

    }

    public Present(Set<Candy> candies) {
        this.candies = candies;
        for (Candy candy : candies) {
            weight += candy.getWeight();
            total_sum += candy.getPrice();
        }

    }

    public Set<Candy> getCandies() {
        return candies;
    }

    public void setCandies(Set<Candy> candies) {
        this.candies = candies;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getTotal_sum() {
        return total_sum;
    }

    public void setTotal_sum(double total_sum) {
        this.total_sum = total_sum;
    }

    public List<Candy> getCandiesBySugarBounds(double l, double r) {
        List<Candy> filteredCandies = new ArrayList<>();
        for (Candy candy : candies) {
            if (candy.getSugar_amount() >= l && candy.getSugar_amount() <= r) {
                filteredCandies.add(candy);
            }
        }
        return filteredCandies;
    }

    @Override
    public String toString() {
        String s = "";
        for (Candy c : candies) {
            s += c;
        }
        return s;
    }


}
